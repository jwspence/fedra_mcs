/********************************************************************
* DataConversionCint.h
* CAUTION: DON'T CHANGE THIS FILE. THIS FILE IS AUTOMATICALLY GENERATED
*          FROM HEADER FILES LISTED IN G__setup_cpp_environmentXXX().
*          CHANGE THOSE HEADER FILES AND REGENERATE THIS FILE.
********************************************************************/
#ifdef __CINT__
#error DataConversionCint.h/C is only for compilation. Abort cint.
#endif
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define G__ANSIHEADER
#define G__DICTIONARY
#include "G__ci.h"
extern "C" {
extern void G__cpp_setup_tagtableDataConversionCint();
extern void G__cpp_setup_inheritanceDataConversionCint();
extern void G__cpp_setup_typetableDataConversionCint();
extern void G__cpp_setup_memvarDataConversionCint();
extern void G__cpp_setup_globalDataConversionCint();
extern void G__cpp_setup_memfuncDataConversionCint();
extern void G__cpp_setup_funcDataConversionCint();
extern void G__set_cpp_environmentDataConversionCint();
}


#include "TROOT.h"
#include "TMemberInspector.h"
#include "libDataConversion.h"
#include <algorithm>
namespace std { }
using namespace std;

#ifndef G__MEMFUNCBODY
#endif

extern G__linked_taginfo G__DataConversionCintLN_vectorlETStreamerInfomUcOallocatorlETStreamerInfomUgRsPgR;
extern G__linked_taginfo G__DataConversionCintLN_reverse_iteratorlEvectorlETStreamerInfomUcOallocatorlETStreamerInfomUgRsPgRcLcLiteratorgR;

/* STUB derived class for protected member access */
