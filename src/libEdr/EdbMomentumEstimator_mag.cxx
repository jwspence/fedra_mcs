//-- Author :  Valeri Tioukov 8/05/2008

//////////////////////////////////////////////////////////////////////////
//                                                                      //
//  Track momentum estimation algorithms                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TMath.h"
#include "TF1.h"
#include "TArrayF.h"
#include "TVectorF.h"
#include "TVector.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "EdbLog.h"
#include "EdbPhys.h"
#include "EdbAffine.h"
#include "EdbMomentumEstimator.h"

ClassImp(EdbMomentumEstimator);

using namespace TMath;

//________________________________________________________________________________________
EdbMomentumEstimator::EdbMomentumEstimator()
{
  eF1	 = 0;
  eF1X   = 0;
  eF1Y   = 0;
  eG	 = 0;
  eGX    = 0;
  eGY    = 0;
  eVerbose=0;
  SetParPMS_Mag(); 
}

//________________________________________________________________________________________
EdbMomentumEstimator::~EdbMomentumEstimator()
{
  SafeDelete(eF1);
  SafeDelete(eF1X);
  SafeDelete(eF1Y);
  SafeDelete(eG);
  SafeDelete(eGX);
  SafeDelete(eGY);
}

//________________________________________________________________________________________
void EdbMomentumEstimator::SetParPMS_Mag()
{
  // set the default values for parameters used in PMS_Mag
  eX0 = 5600;
  eFlagt=0;

  eDT0  = 0.0021;
  eDT1  = 0.0054; 
  eDT2  = 0.;

 // eDTx0 =  0.00156;
  eDTx0 =  0.0021;
  eDTx1 =  0.0093;
  eDTx2 = 0.;

 // eDTy0 =  0.00156;
  eDTy0 =  0.0021;
  eDTy1 =  0.; 		// detheta_T does not depend on the angle (transversal coordinate called "y" in this code)
  eDTy2 = 0.;
}

//________________________________________________________________________________________
void EdbMomentumEstimator::Print()
{
  printf("EdbMomentumEstimator:\n");
  printf("eX0 = %f \n", eX0);
  printf("eDT0,  eDT1,  eDT2 = %f %f %f\n", eDT0, eDT1, eDT2);
  printf("eDTx0, eDTx1, eDTx2 = %f %f %f\n", eDTx0, eDTx1, eDTx2);
  printf("eDTy0, eDTy1, eDTy2 = %f %f %f\n", eDTy0, eDTy1, eDTy2);
}

//________________________________________________________________________________________
float EdbMomentumEstimator::PMSang(EdbTrackP tr)
{
  // Version rewised by VT 13/05/2008
  //
  // Momentum estimation by multiple scattering (Annecy implementation Oct-2007)
  //
  // Input: tr       - track
  //                   pass track by value - so we can modify the local copy
  //
  // calculate momentum in transverse and in longitudinal projections using the different 
  // measurements errors parametrisation

   
  int nseg = tr.N();
  int npl=tr.Npl();
  if(nseg<2)   { Log(1,"PMSang","Warning! nseg<2 (%d)- impossible estimate momentum!",nseg);             return -99;}
  if(npl<nseg) { Log(1,"PMSang","Warning! npl<nseg (%d, %d) - use track.SetCounters() first",npl,nseg);  return -99;}
  int plmax = Max( tr.GetSegmentFirst()->PID(), tr.GetSegmentLast()->PID() ) + 1;
  if(plmax<1||plmax>1000)   { Log(1,"PMSang","Warning! plmax = %d - correct the segments PID's!",plmax); return -99;}

 
  float xmean,ymean,zmean,txmean,tymean,wmean;
  float xmean0,ymean0,zmean0,txmean0,tymean0,wmean0;
  FitTrackLine(tr,xmean0,ymean0,zmean0,txmean0,tymean0,wmean0);    // calculate mean track parameters
  float tmean=sqrt(txmean0*txmean0+ tymean0*tymean0);
  
  EdbSegP *aas;
  float sigmax=0, sigmay=0;
  for (int i =0;i<tr.N();i++)
  {
   aas=tr.GetSegment(i); 
   sigmax+=(txmean0-aas->TX())*(txmean0-aas->TX());   
   sigmay+=(tymean0-aas->TY())*(tymean0-aas->TY());   
  } 
  sigmax=1/tr.N()*Sqrt(sigmax);
  sigmay=1/tr.N()*Sqrt(sigmay);
  for (int i =0;i<tr.N();i++)
  {
   aas=tr.GetSegment(i);
   if(Abs(aas->TX()-sigmax)>3) { aas->Set(aas->ID(),aas->X(),aas->Y(),0.,aas->TY(),aas->W(),aas->Flag());}
   if(Abs(aas->TY()-sigmay)>3) { aas->Set(aas->ID(),aas->X(),aas->Y(),aas->TX(),0.,aas->W(),aas->Flag());}  
  } 

  FitTrackLine(tr,xmean0,ymean0,zmean0,txmean0,tymean0,wmean0);

//  EdbAffine2D aff;
//  aff.ShiftX(-xmean0);
//  aff.ShiftY(-ymean0);
//  aff.Rotate( -ATan2(txmean0,tymean0) );   // rotate track to get longitudinal as tx, transverse as ty angle
//  tr.Transform(aff);
  float PHI=atan2(txmean0,tymean0);
  for (int i =0;i<tr.N();i++)
  {
   aas=tr.GetSegment(i);
   float slx=aas->TY()*cos(-PHI)-aas->TX()*sin(-PHI);
   float sly=aas->TX()*cos(-PHI)+ aas->TY()*sin(-PHI);
   aas->Set(aas->ID(),aas->X(),aas->Y(),slx,sly,aas->W(),aas->Flag());
  }
  
  FitTrackLine(tr,xmean,ymean,zmean,txmean,tymean,wmean);    // calculate mean track parameters
  



  // -- start calcul --

  int minentr  = 4;               // min number of entries in the cell to accept the cell for fitting
  int stepmax  = npl-1; //npl/minentr;     // max step
  const int size     = stepmax+1;       // vectors size

  TVectorF da(size), dax(size), day(size);
  TArrayI  nentr(size), nentrx(size), nentry(size);

  EdbSegP *s1,*s2;
  for(int ist=1; ist<=stepmax; ist++)         // cycle by the step size
    {
      for(int i1=0; i1<nseg-1; i1++)          // cycle by the first seg
	{
	  s1 = tr.GetSegment(i1);
	  if(!s1) continue;
	  for(int i2=i1+1; i2<nseg; i2++)      // cycle by the second seg
	    {
	      s2 = tr.GetSegment(i2);
	      if(!s2) continue;
	      int icell = Abs(s2->PID()-s1->PID());
	      if( icell == ist ) 
	      {
		if (s2->TX()!=0&&s1->TX()!=0)
		  { 
		   dax[icell-1]   += ( (ATan(s2->TX())- ATan(s1->TX())) * (ATan(s2->TX())- ATan(s1->TX())) );
		   nentrx[icell-1]+=1;
		  }
		 if (s2->TY()!=0&&s1->TY()!=0)
		  { 
		   day[icell-1]   += ( (ATan(s2->TY())- ATan(s1->TY())) * (ATan(s2->TY())- ATan(s1->TY())) );
		   nentry[icell-1]+=1;		   
		  }
		 if (s2->TX()!=0&&s1->TX()!=0&&s2->TY()!=0&&s1->TY()!=0)
		  {
		   da[icell-1]   += (( (ATan(s2->TX())- ATan(s1->TX())) * (ATan(s2->TX())- ATan(s1->TX())) ) 
		                     + ( (ATan(s2->TY())-ATan(s1->TY())) * (ATan(s2->TY())- ATan(s1->TY())) ));
		   nentr[icell-1] +=1;
		  }
	      }
	    }
	}
    }
 
  float Zcorr = Sqrt(1+txmean0*txmean0+tymean0*tymean0);  // correction due to non-zero track angle and crossed lead thickness

  int maxX =0, maxY=0, max3D=0;                                  // maximum value for the function fit
  TVectorF vindx(size), errvindx(size),vindy(size), errvindy(size),vind3d(size), errvind3d(size);
  TVectorF errda(size), errdax(size), errday(size);
  int ist=0,  ist1=0, ist2=0;                          // use the counter for case of missing cells 
  for(int i=0; i<size; i++) 
    {
      if( nentrx[i] >= minentr && Abs(dax[i])<0.1) 
      {
	vindx[ist]    = i+1;                            // x-coord is defined as the number of cells
	errvindx[ist] = .25;
	dax[ist]    = Sqrt( dax[i]/(nentrx[i]*Zcorr) );
	errdax[ist] = dax[ist]/sqrt(2*nentrx[i]);//CellWeight(npl,i+1);    //   Sqrt(npl/vind[i]);
        ist++;
        maxX=ist;
      }
      if( nentry[i] >= minentr && Abs(day[i])<0.1) 
      {
	vindy[ist1]    = i+1;                            // x-coord is defined as the number of cells
	errvindy[ist1] = .25;
	day[ist1]    = Sqrt( day[i]/(nentry[i]*Zcorr) );
	errday[ist1] = day[ist1]/sqrt(2*nentry[i]);//CellWeight(npl,i+1);
     	ist1++;
        maxY=ist1;
      }      
      if( nentr[i] >= minentr/2 && Abs(da[i])<0.1 ) 
      {
	vind3d[ist2]    = i+1;                            // x-coord is defined as the number of cells
	errvind3d[ist2] = .25;
	da[ist2]    = Sqrt( da[i]/(2*nentr[i]*Zcorr) );
	errda[ist2] = da[ist2]/sqrt(4*nentr[i]);//CellWeight(npl,i+1));	
	ist2++;
	max3D=ist2;
       }
    }

  float dt = eDT0 + eDT1*Abs(tmean) + eDT2*tmean*tmean;  // measurements errors parametrization
  dt*=dt;
  float dtx = eDTx0 + eDTx1*Abs(txmean) + eDTx2*txmean*txmean;  // measurements errors parametrization
  dtx*=dtx;
  float dty = eDTy0 + eDTy1*Abs(tymean) + eDTy2*tymean*tymean;  // measurements errors parametrization
  dty*=dty;
  
  float x0    = eX0/1000;      

  SafeDelete(eF1);
  SafeDelete(eF1X);
  SafeDelete(eF1Y);
  SafeDelete(eG);
  SafeDelete(eGX);
  SafeDelete(eGY);

  eF1X = MCSErrorFunction("eF1X",x0,dtx);    eF1X->SetRange(0,14);
  eF1X->SetParameter(0,2000.);                             // starting value for momentum in GeV
  eF1Y = MCSErrorFunction("eF1Y",x0,dty);    eF1Y->SetRange(0,14);
  eF1Y->SetParameter(0,2000.);                             // starting value for momentum in GeV
  eF1 = MCSErrorFunction("eF1",x0,dt);     eF1->SetRange(0,14);
  eF1->SetParameter(0,2000.);                             // starting value for momentum in GeV

if (max3D>0)
{
  eG=new TGraphErrors(vind3d,da,errvind3d,errda);
  eG->Fit("eF1","MQR");
  eP=1./1000.*Abs(eF1->GetParameter(0));
  eDP=1./1000.*eF1->GetParError(0);
  if (eP>20||eP<0||eP==2) eP=-99;
  EstimateMomentumError( eP, npl, tmean, ePmin, ePmax );
  if (eVerbose) printf("P3D=%7.2f GeV ; 90%%C.L. range = [%6.2f : %6.2f] \n", eP, ePmin, ePmax);
}
if (maxX>0)
{
  eGX=new TGraphErrors(vindx,dax,errvindx,errdax);
  eGX->Fit("eF1X","MQR");
  ePx=1./1000.*Abs(eF1X->GetParameter(0));
  eDPx=1./1000.*eF1X->GetParError(0);
  if (ePx>20||ePx<0||ePx==2) ePx=-99;
  EstimateMomentumError( ePx, npl, txmean, ePXmin, ePXmax );
  if (eVerbose) printf("PL=%7.2f GeV ; 90%%C.L. range = [%6.2f : %6.2f] \n",ePx,ePXmin, ePXmax);
}
if (maxY>0)
{
  eGY=new TGraphErrors(vindy,day,errvindy,errday);
  eGY->Fit("eF1Y","MQR");
  ePy=1./1000.*Abs(eF1Y->GetParameter(0));
  eDPy=1./1000.*eF1Y->GetParError(0);
  if (ePy>20||ePy<0||ePy==2) ePy=-99;
  EstimateMomentumError( ePy, npl, tymean, ePYmin, ePYmax ); 
  if (eVerbose) printf("PT=%7.2f GeV ; 90%%C.L. range = [%6.2f : %6.2f] \n", ePy, ePYmin, ePYmax);
}
 
  float ptrue=eP;

//-----------------------------TO TEST with MC studies------------------------------------
//  float wx = 1./eDPx/eDPx;
//  float wy = 1./eDPy/eDPy;
//  float ptest  = (ePx*wx + ePy*wy)/(wx+wy);  
//----------------------------------------------------------------------------------------

  return ptrue;
}

//________________________________________________________________________________________
float EdbMomentumEstimator::CellWeight(int npl, int m)
{
//--------------------------- TO BE IMPLEMENTED-----------------------------------------

  // npl - number of plates, m - the cell thickness in plates
  // return the statistical weight of the cell

  //return  Sqrt(npl/m);  // the simpliest estimation no shift, no correlations

  return 2*Sqrt( npl/m + 1./m/m*( npl*(m-1) - m*(m-1)/2.) );
 // return 1;
}

//________________________________________________________________________________________
TF1 *EdbMomentumEstimator::MCSErrorFunction(const char *name, float x0, float dtx)
{
  //        dtx - the plane angle measurement error
  // return the function of the expected angular deviation vs range
  //
  // use the Highland-Lynch-Dahl formula for theta_rms_plane = 13.6 MeV/bcp*z*sqrt(x/x0)*(1+0.038*log(x/x0))  (PDG)
  // so the expected measured angle is sqrt( theta_rms_plane**2 + dtx**2)
  //
  // The constant term im the scattering formula is not 13.6 but 14.64, which
  // is the right reevaluated number, due to a calculation with the moliere
  // distribution. 13.6 is an approximation. See Geant3 or 4 references for more explanations.???????
  //
  // err(x) = sqrt(k*x*(1+0.038*log(x/x0))/p**2 + dtx)

  //  float k   = 14.64*14.64/x0;
  // 14.64*14.64/1000/1000 = 0.0002143296  - we need p in GeV
  // 13.6*13.6/1000/1000   = 0.0001849599  - we need p in GeV


   return new TF1(name,Form("sqrt(214.3296*x/%f*(1+0.038*log(x/(%f)))/([0])**2+%f)",x0,x0,dtx));

  //P is returned in MeV by this function for more convinience, but given in GeV as output.
}
//______________________________________________________________________________________
void EdbMomentumEstimator::EstimateMomentumError(float P, int npl, float ang, float &pmin, float &pmax)
{
 if (P<0) {pmin=-99; pmax=-99; return;}
 
  float pinv=1./P;
  float  DP=Mat(P, npl, ang );
  float pinvmin=pinv*(1-DP*1.64);
  float pinvmax=pinv*(1+DP*1.64);
  pmin=(1./pinvmax);   //90%CL minimum momentum
  pmax=(1./pinvmin);   //90%CL maximum momentum
  if (P>20.||pmax>20.) {pmax=1000000.;}
}
//______________________________________________________________________________________
double EdbMomentumEstimator::Mat(float P, int npl, float ang)
{
 // These parametrisations at low and large angles are parametrised with MC
 // See Magali's thesis for more informations

 double DP=0.;
 
 if(Abs(ang)<0.1)  DP=(0.61+0.016*P)+(-0.12+0.0029*P)*Sqrt(npl)+npl*(0.0078-0.0004*P);
 if(Abs(ang)>=0.1)  DP=(1.22-0.012*P)+(-0.32+0.021*P)*Sqrt(npl)+npl*(0.023-0.0022*P);
 
 if (DP>0.60) DP=0.60;
 
 return DP;
}
//______________________________________________________________________________________
