//-- Author :  Valeri Tioukov   11/06/2008

#include <string.h>
#include <iostream>
#include <TEnv.h>
#include "EdbLog.h"
#include "EdbScanProc.h"

using namespace std;

void print_help_message()
{
  cout<< "\nUsage: \n\t  emalign -AidA  -BidB[-pNPRE -fNFULL -oDATA_DIRECTORY -vDEBUG] \n";
  cout<< "\t  emalign -setID [-pNPRE -fNFULL -oDATA_DIRECTORY -vDEBUG -m] \n\n";
  cout<< "\t\t  idA   - id of the first piece formed as BRICK.PLATE.MAJOR.MINOR \n";
  cout<< "\t\t  idB   - id of the second piece formed as BRICK.PLATE.MAJOR.MINOR \n";
  cout<< "\t\t  ID    - id of the dataset formed as BRICK.PLATE.MAJOR.MINOR \n";
  cout<< "\t\t  NPRE  - number of the prealignments (default is 0)\n";
  cout<< "\t\t  NFULL - number of the fullalignments (default is 0)\n";
  cout<< "\t\t  DEBUG - verbosity level: 0-print nothing, 1-errors only, 2-normal, 3-print all messages\n";
  cout<< "\t\t  -m    - make the affine files starting from EdbScanSet\n";
  cout<< "\nExample: \n";
  cout<< "\t  o2root -id4554.10.1.0 -o/scratch/BRICKS \n";
  cout<< "\nThe data location directory if not explicitly defined will be taken from .rootrc as: \n";
  cout<< "\t  emrec.outdir:      /scratch/BRICKS \n";
  cout<< "\t  emrec.EdbDebugLevel:      1\n";
  cout<<endl;
}

int main(int argc, char* argv[])
{
  if (argc < 2)   { print_help_message();  return 0; }
  
  const char *outdir    = gEnv->GetValue("emrec.outdir"   , "./");
  gEDBDEBUGLEVEL        = gEnv->GetValue("emrec.EdbDebugLevel" , 1);

  bool      do_ida      = false;
  bool      do_idb      = false;
  bool      do_set      = false;
  bool      do_makeAff  = false;
  Int_t     brick=0, plate=0, major=0, minor=0;
  Int_t     npre=0,  nfull=0;
  EdbID     idA,idB;

  for(int i=1; i<argc; i++ ) {
    char *key  = argv[i];

    if     (!strncmp(key,"-A",2)) 
      {
	if(strlen(key)>2)	sscanf(key+2,"%d.%d.%d.%d",&brick,&plate,&major,&minor);
	idA.Set(brick,plate,major,minor);
	do_ida=true;
      }
    else if(!strncmp(key,"-B",2)) 
      {
	if(strlen(key)>2)	sscanf(key+2,"%d.%d.%d.%d",&brick,&plate,&major,&minor);
	idB.Set(brick,plate,major,minor);
	do_idb=true;
      }
    else if(!strncmp(key,"-set",4))
      {
	if(strlen(key)>4)	sscanf(key+4,"%d.%d.%d.%d",&brick,&plate,&major,&minor);
	do_set=true;
      }
    else if(!strncmp(key,"-o",2)) 
      {
	if(strlen(key)>2)	outdir=key+2;
      }
    else if(!strncmp(key,"-p",2))
      {
	if(strlen(key)>2)	npre = atoi(key+2);
      }
    else if(!strncmp(key,"-f",2))
      {
	if(strlen(key)>2)	nfull = atoi(key+2);
      }
    else if(!strncmp(key,"-v",2))
      {
	if(strlen(key)>2)	gEDBDEBUGLEVEL = atoi(key+2);
      }
    else if(!strncmp(key,"-m",2))
      {
	do_makeAff=true;
      }
  }

  if(!((do_ida&&do_idb)||do_set))   { print_help_message(); return 0; }

  EdbScanProc sproc;
  sproc.eProcDirClient=outdir;
  if(do_ida&&do_idb) {
    printf("\n----------------------------------------------------------------------------\n");
    printf("align  %d.%d.%d.%d and  %d.%d.%d.%d\n"
	   ,idA.eBrick,idA.ePlate, idA.eMajor,idA.eMinor
	   ,idB.eBrick,idB.ePlate, idB.eMajor,idB.eMinor
	   );
    printf("----------------------------------------------------------------------------\n\n");

    sproc.AlignAll(idA,idB, npre, nfull);
  }
  if(do_set) {
    printf("\n----------------------------------------------------------------------------\n");
    printf("align set %d.%d.%d.%d\n", brick,plate, major,minor);
    printf("----------------------------------------------------------------------------\n\n");

    EdbID id(brick,plate,major,minor);
    EdbScanSet *ss = sproc.ReadScanSet(id);
    ss->Brick().SetID(brick);
    ss->MakePIDList();
    if(do_makeAff) sproc.MakeAFFSet(*ss);
    if(ss) sproc.AlignSet(*ss, npre, nfull);
  }

  return 1;
}
