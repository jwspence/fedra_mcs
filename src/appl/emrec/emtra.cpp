//-- Author :  Valeri Tioukov   11/06/2008

#include <string.h>
#include <iostream>
#include <TEnv.h>
#include "EdbLog.h"
#include "EdbScanProc.h"

using namespace std;

void print_help_message()
{
  cout<< "\nUsage: \n\t  emtra -set=ID [ -o=DATA_DIRECTORY -v=DEBUG] \n\n";
  cout<< "\t\t  ID    - id of the dataset formed as BRICK.PLATE.MAJOR.MINOR \n";
  cout<< "\t\t  DEBUG - verbosity level: 0-print nothing, 1-errors only, 2-normal, 3-print all messages\n";
  cout<< "\t\t  -m    - make the affine files starting from EdbScanSet\n";
  cout<< "\nExample: \n";
  cout<< "\t  emtra -id=4554.10.1.0 -o/scratch/BRICKS \n";
  cout<< "\nThe data location directory if not explicitly defined will be taken from .rootrc as: \n";
  cout<< "\t  emrec.outdir:      /scratch/BRICKS \n";
  cout<< "\t  emrec.EdbDebugLevel:      1\n";
  cout<<endl;
}

int main(int argc, char* argv[])
{
  if (argc < 2)   { print_help_message();  return 0; }
  
  const char *outdir    = gEnv->GetValue("emrec.outdir"   , "./");
  gEDBDEBUGLEVEL        = gEnv->GetValue("emrec.EdbDebugLevel" , 1);

  bool      do_set      = false;
  Int_t     brick=0, plate=0, major=0, minor=0;

  for(int i=1; i<argc; i++ ) {
    char *key  = argv[i];

    if(!strncmp(key,"-set=",5))
      {
	if(strlen(key)>5)	sscanf(key+5,"%d.%d.%d.%d",&brick,&plate,&major,&minor);
	do_set=true;
      }
    else if(!strncmp(key,"-o=",3)) 
      {
	if(strlen(key)>3)	outdir=key+3;
      }
    else if(!strncmp(key,"-v=",3))
      {
	if(strlen(key)>3)	gEDBDEBUGLEVEL = atoi(key+3);
      }
  }

  if(!do_set)   { print_help_message(); return 0; }

  EdbScanProc sproc;
  sproc.eProcDirClient=outdir;
  if(do_set) {
    printf("\n----------------------------------------------------------------------------\n");
    printf("tracking set %d.%d.%d.%d\n", brick,plate, major,minor);
    printf("----------------------------------------------------------------------------\n\n");

    EdbID id(brick,plate,major,minor);
    EdbScanSet *ss = sproc.ReadScanSet(id);
    ss->Brick().SetID(brick);
    //ss->MakePIDList();
    sproc.AssembleScanSet(*ss);

    TCut c="s.eW>13&&eCHI2P<2.5&&s1.eFlag>=0&&s2.eFlag>=0&&eN1==1&&eN2==1";

    EdbScanCond cond;
    cond.Print();
    sproc.TrackSetBT(*ss,cond,c);

  }

  return 1;
}
