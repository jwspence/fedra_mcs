#ifndef __EDA_ShowerTab_H__
#define __EDA_ShowerTab_H__

#include "EdbEDA.h"
#include "EdbShowerRec.h"
#include "EdbShowAlg.h"
#include <TG3DLine.h>


class EdbEDAShowerTab {
  private:
    
    // The ShowerRec object:
    EdbShowerRec* eShowerRec;
    
    // The InitiatorBT array. Has to be variable otherwise it 
    // will be out of scope:
    TObjArray* eInBTArray;

   // Two Pointers for the EdbPVRec object:
   EdbPVRec* pvrec_linkedtracks;
   EdbPVRec* pvrec_cpfiles;


    
    // Buttons and so on:
    TGComboBox* eComboBox_Parameter;
    TGCheckButton* eCheckButtonUseTSData;
    TGCheckButton* eCheckButtonDrawShowers;
    TGCheckButton* eCheckButtonCheckQualitycut;
    TGNumberEntry* eNumberEntry_ParaValue;
    
    TGNumberEntry* eTGNumberEntryDrawShowerNr;
    
    // Various Variables used for settings:
    Bool_t eUseTSData;
    Bool_t eIsAliLoaded;
    
    Bool_t eDrawShowers;
		Bool_t eCheckQualitycut;
    
// An own Shower array. Not the one of the ShowerRec object:
    Int_t  eNShowers;
   TObjArray* RecoShowerArray;
    
    
  public:
  
    EdbEDAShowerTab();
  
    // -----------------------------------
    void MakeGUI();
    void LoadShowerRecEdbPVRec();
		void CheckBTDensity(EdbPVRec *pvr, TObjArray *tracks);
		void CheckBTDensityCanv(EdbPVRec *pvr = NULL, TObjArray *tracks = NULL);
		void CheckQualitycut();
    
    void SetInBTFromSelected();
    void SetInBTFromLinkedTracks()        { eShowerRec->SetInBTArray(NULL);} /// TODO
    void CheckInBT();  
    
    void Reco();
    void Reset();

		void 		FindPairings();
		void 		FindPairingsNewAlgo();
		Bool_t 	CheckPairDuplications(Int_t nmax, Int_t SegPID,Int_t SegID,Int_t Seg2PID,Int_t Seg2ID,TArrayI* SegmentPIDArray,TArrayI* SegmentIDArray,TArrayI* Segment2PIDArray,TArrayI* Segment2IDArray);

    void PrintShowerRecInBT()             {cout << "DBG1"<<endl;eShowerRec->PrintInitiatorBTs();cout << "DBG2"<<endl;return;}
    void PrintShowerRecRecoShowerArray()  {eShowerRec->PrintRecoShowerArray();return;}
    
    void ChangeShowerRecParameters();
    void PrintShowerRecParameters()       {eShowerRec->PrintParameters();return;}  
    void ResetShowerRecParameters()       {eShowerRec->ResetAlgoParameters();return;}  

    void SetParaSetHighNBT();
    void SetParaSetHighPur();
    
    void ReadComboBoxParameter();
    void SetNumberEntryShowerRecParameter(Double_t val);
    
    
    
    
    void DrawShower(int iShowerNr,bool eClearShower=0);
    void DrawShower();
		void DrawShowerAll();
    void DrawSingleShower(EdbTrackP* shower);
    void DrawShowerPrevious();
    void DrawShowerNext();
    
    void DrawShowers(); // For the toggle Button!
    
    void Button1(){ printf("****************   NOT IMPLEMETED    *************************\n");}
    void Button2(){ printf("Button2\n");}
    void Button3(){ PrintShower(); printf("Print\n");}
    
    void PrintShower();
		//-------------------------------------------------------------------------------------------
		// Usually already decleared in EdbEdaPlotTab, but since this is for now only of interest
		// of shower tab we redaclarate it here....
		TCanvas * CreateCanvas(char *plot_name){
				// --- Create an embedded canvas
				gEve->GetBrowser()->StartEmbedding(1);
				gROOT->ProcessLineFast("new TCanvas");
				TCanvas *c1 = (TCanvas*) gPad;
				gEve->GetBrowser()->StopEmbedding(plot_name);
				return c1;
		}

		double CalcdR_NoPropagation(EdbSegP* s1,EdbSegP* stest);
		double CalcdR_WithPropagation(EdbSegP* s1,EdbSegP* stest);
		double CalcdTheta(EdbSegP* s1,EdbSegP* s2);

	ClassDef(EdbEDAShowerTab,0) // Tab for accessibililty to Shower-rec.
    
};




#endif //__EDA_ShowerTab_H__

