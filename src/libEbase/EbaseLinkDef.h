#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class EdbLayer;
#pragma link C++ class EdbPlateP;
#pragma link C++ class EdbBrickP;

#pragma link C++ class EdbScanCond;
#pragma link C++ class EdbSegmentCut;

#pragma link C++ class EdbID;
#pragma link C++ class EdbScanSet;

#pragma link C++ class EdbSegP;

#endif
