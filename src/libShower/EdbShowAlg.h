#ifndef ROOT_EdbShowAlg
#define ROOT_EdbShowAlg

#include "TROOT.h"
#include "TFile.h"
#include "TVector3.h"
#include "TIndexCell.h"
#include "TArrayF.h"
#include "TBenchmark.h"
#include "EdbVertex.h"
#include "EdbPVRec.h"
#include "EdbPhys.h"
#include "EdbMath.h"
#include "EdbMomentumEstimator.h"
#include "EdbLog.h"
#include "EdbPattern.h"
using namespace std;

//______________________________________________________________________________

class EdbShowAlg : public TObject {
  
  private:
// nothing...
	
	protected:
		
		// A algorithm here should not have more than 10 different parameters, otherwise we
		// will have to use TObjArray implementations.
		TString      eAlgName;
		Int_t        eAlgValue;
		Float_t      eParaValue[10];
		TString      eParaString[10];
		Int_t				 eActualAlgParametersetNr; // Used when more sets of same algorithm
		

  		// Pointer copies to be used for reconstruction of showers
		// EdbPVRec object:
		EdbPVRec*          eAli;
		Int_t              eAliNpat;
    // TObjArray storing Initiator Basetracks:
		TObjArray*         eInBTArray;
		Int_t              eInBTArrayN;
		// Variables describing plate number cuts.
		Int_t              eFirstPlate_eAliPID;
		Int_t              eLastPlate_eAliPID;
		Int_t              eMiddlePlate_eAliPID;
		Int_t              eNumberPlate_eAliPID;
    // TObjArray storing Reconstructed Showers:
    TObjArray*         eRecoShowerArray;
    Int_t              eRecoShowerArrayN;
    
	   // Transformed (smaller) EdbPVRec object:
    EdbPVRec*          eAli_Sub;
    Int_t              eAli_SubNpat;
    // Flag wheter to use the small EdbPvrec Object or not. Using small object, this will not be deleted ue to 
    // pointer problems. See in implementation of Transform() for details.
    Int_t               eUseAliSub;
    
        
    
    
		// In constructor this is created on heap...
		//EdbShowerP*	        eRecoShower;
		EdbTrackP*	        eRecoShower;
		
		// Reset All Default Variables:
		void 								Set0();
		
		
  public:
        
    EdbShowAlg();
		EdbShowAlg(TString AlgName, Int_t AlgValue);
// 		EdbShowAlg(TString AlgName, Int_t AlgValue, Float_t* ParaValue[10], TString ParaString[10]);
// 		EdbShowAlg(EdbPVRec* gAli, TObjArray* InBTArray);
		
   		
		virtual ~EdbShowAlg();          // virtual constructor due to inherited class
        
        
		// Hand over  eAli  eInBTArray  from EdbShowerRec
		inline void         SetEdbPVRec( EdbPVRec* Ali )          { eAli = Ali; eAliNpat=eAli->Npatterns(); }
		inline void         SetInBTArray( TObjArray* InBTArray ) { eInBTArray = InBTArray; eInBTArrayN=eInBTArray->GetEntries(); }
		inline void         SetEdbPVRecPIDNumbers(Int_t FirstPlate_eAliPID, Int_t LastPlate_eAliPID, Int_t MiddlePlate_eAliPID, Int_t NumberPlate_eAliPID) {
			eFirstPlate_eAliPID=FirstPlate_eAliPID;
			eLastPlate_eAliPID=LastPlate_eAliPID;
			eMiddlePlate_eAliPID=MiddlePlate_eAliPID;
			eNumberPlate_eAliPID=NumberPlate_eAliPID;
		}
    inline void         SetRecoShowerArray(TObjArray* RecoShowerArray) { eRecoShowerArray=RecoShowerArray; }
    inline void         SetRecoShowerArrayN(Int_t RecoShowerArrayN) { eRecoShowerArrayN=RecoShowerArrayN; }
		
		
		inline void 				SetActualAlgParameterset(Int_t ActualAlgParametersetNr) {eActualAlgParametersetNr=ActualAlgParametersetNr;}
		
    inline Int_t        GetAlgValue()  const { return eAlgValue;}
		inline TString      GetAlgName()  const { return eAlgName;}
		inline Int_t        GetRecoShowerArrayN()     const       { return eRecoShowerArrayN; }
		inline TObjArray*   GetRecoShowerArray()     const       { return eRecoShowerArray; }
    
    inline void         SetUseAliSub(Bool_t UseAliSub) { eUseAliSub=UseAliSub; }
    
    Double_t            DeltaR_WithPropagation(EdbSegP* s,EdbSegP* stest);
    Double_t            DeltaR_WithoutPropagation(EdbSegP* s,EdbSegP* stest);
    Double_t            DeltaR_NoPropagation(EdbSegP* s,EdbSegP* stest);
    
    Double_t            DeltaTheta(EdbSegP* s1,EdbSegP* s2);
    Double_t            DeltaThetaComponentwise(EdbSegP* s1,EdbSegP* s2);
    Double_t            DeltaThetaSingleAngles(EdbSegP* s1,EdbSegP* s2); 
    Double_t            GetSpatialDist(EdbSegP* s1,EdbSegP* s2);
    Double_t 		GetMinimumDist(EdbSegP* seg1,EdbSegP* seg2);
    
    
    void         	SetParameters(Float_t* par);
    //void                Transform_eAli(EdbSegP* InitiatorBT);
    void                Transform_eAli( EdbSegP* InitiatorBT, Float_t ExtractSize);
    Bool_t              IsInConeTube(EdbSegP* sTest, EdbSegP* sStart, Double_t CylinderRadius, Double_t ConeAngle);
    
    
    void Print();
    void PrintParameters();
    void PrintParametersShort();
    void PrintMore();
    void PrintAll();


    // Main functions for using this ShowerAlgorithm Object.
    // Structure is made similar to OpRelease, where 
    //  Initialize, Execute, Finalize 
    // give the three columns of the whole thing.
    // Since these functions depend on the algorithm type they are made virtual
    // and implemented in the inherited classes.
    virtual void Initialize();
		virtual void Execute();
		virtual void Finalize();
    
    ClassDef(EdbShowAlg,1);         // Root Class Definition for my Objects
};


//______________________________________________________________________________

class EdbShowAlg_GS : public EdbShowAlg {
  
  private:
		// TObjArray storing Initiator Vertices:
		TObjArray*         eInVtxArray;
		Int_t              eInVtxArrayN;
  public:
        
    EdbShowAlg_GS();
    virtual ~EdbShowAlg_GS();          // virtual constructor due to inherited class

    void    Init();
    
		inline void         SetInVtxArray( TObjArray* InVtxArray ) { eInVtxArray = InVtxArray; eInVtxArrayN = eInVtxArray->GetEntries(); cout << eInVtxArrayN << "  entries set"<<endl; }
		void         	    SetInVtx( EdbVertex* vtx ); 

		inline Int_t        GetInVtxArrayN()     	const       { return eInVtxArrayN; }
		inline TObjArray*   GetInVtxArray()     	const       { return eInVtxArray; }


		// Helper Functions for this class:
		void 		Convert_InVtxArray_To_InBTArray();
		Bool_t		CheckPairDuplications(Int_t SegPID,Int_t SegID,Int_t Seg2PID,Int_t Seg2ID,TArrayI* SegmentPIDArray,TArrayI* SegmentIDArray,TArrayI* Segment2PIDArray,TArrayI* Segment2IDArray);
		Double_t 	CalcIP(EdbSegP *s, double x, double y, double z);
		Double_t 	CalcIP(EdbSegP *s, EdbVertex *v);
		Bool_t 		IsPossibleFakeDoublet(EdbSegP* s1,EdbSegP* s2);
		
		
		TObjArray* 	FindPairs(EdbSegP* InBT, EdbPVRec* eAli_Sub);
		TObjArray* 	CheckCleanPairs(EdbSegP* InBT, TObjArray* RecoShowerArrayFromFindPairs);
    
    // Main functions for using this ShowerAlgorithm Object.
    // Structure is made similar to OpRelease, where 
    //  Initialize, Execute, Finalize 
    // give the three columns of the whole thing.
    void Initialize();
    void Execute();
    void Finalize();
    
    ClassDef(EdbShowAlg_GS,1);         // Root Class Definition for my Objects
};

//______________________________________________________________________________

#endif /* ROOT_EdbShowAlg */
