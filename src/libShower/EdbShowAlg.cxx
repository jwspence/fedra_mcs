#include "EdbShowAlg.h"
// #include "EdbShowerP.h"

using namespace std;
using namespace TMath;

ClassImp(EdbShowAlg)
ClassImp(EdbShowAlg_GS)

//______________________________________________________________________________
    
    
		
EdbShowAlg::EdbShowAlg()
{
  // Default Constructor
	cout << "EdbShowAlg::EdbShowAlg()   Default Constructor"<<endl;
	
	// Reset all:
	Set0();
}

//______________________________________________________________________________
    
EdbShowAlg::EdbShowAlg(TString AlgName, Int_t AlgValue)
{
	// Reset all:
	Set0();
	
	eAlgName=AlgName;
	eAlgValue=AlgValue;
	for (int i=0; i<10; ++i) { eParaValue[i]=-99999.0; eParaString[i]="UNSPECIFIED";}
	eAli_Sub=0;
}

//______________________________________________________________________________

EdbShowAlg::~EdbShowAlg()
{
  // Default Destructor
	cout << "EdbShowAlg::~EdbShowAlg()"<<endl;
}

//______________________________________________________________________________

		
void EdbShowAlg::Set0()
{
  // Set0()
	eAlgName="UNSPECIFIED";
	eAlgValue=-999999;
	for (int i=0; i<10; ++i) { eParaValue[i]=-99999.0; eParaString[i]="UNSPECIFIED";}
	eAli_Sub=0;
	
	// do not use use small eAli Object by default:
	// (this solution is memory safe....)
	eUseAliSub=0;
	
	eActualAlgParametersetNr=0;
	
	eRecoShowerArrayN=0;
	eRecoShowerArray=NULL;
	eInBTArrayN=0;
	eInBTArray=NULL;
}

//______________________________________________________________________________

void EdbShowAlg::SetParameters(Float_t* par)
{
  // SetParameters
	for (int i=0; i<10; ++i) { eParaValue[i]=par[i]; }
	cout << "EdbShowAlg::SetParameters()...done"<<endl;
}

//______________________________________________________________________________



 
 
 

//______________________________________________________________________________

void EdbShowAlg::Transform_eAli(EdbSegP* InitiatorBT, Float_t ExtractSize=1500)
{
// 	cout << "-----------------   void EdbShowAlg::Transform_eAli(EdbSegP* InitiatorBT, Float_t ExtractSize=1500)   ------------------"<<endl;
		// 		Transform eAli to eAli_Sub:
		// 		the lenght of eAli_Sub is not changed.
		// 		Only XY-size (and MC) cuts are applied.

		// --------------------------------------------------------------------------------------
		// ---
		// --- Whereas in ShowRec.cpp the treebranch file is written directly after each
		// --- BT reconstruction, it was not a problem when the eAliSub was deleted each time.
		// --- But now situation is different, since the BTs of the showers have the adresses from 
		// --- the eAli_Sub !each! so if one eAli_Sub is deleted the EdbShowerP object has lost its
		// --- BT adresses.
		// --- So if we do not delete the eAli_Sub, reconstrtuction is fast (compared to eAli) but 
		// --- memory increases VERY fast.
		// --- If we do use eAli, then memory consumption will not increase fast, but 
		// --- reconstruction is slow.
		// ---
		// --- Possible workarounds:
		// ---	For few InBTs (EDA use for data...): 		use eAliSub and dont delete it.
		// ---	For many InBTs (parameterstudies):			use eAli---.
		// ---	Use always eAliSub, search BT correspond in eAli, add BT from eAli....May take also long time...
		// ---
		// ---
		// ---
		// ---
		// --------------------------------------------------------------------------------------

		
		/*
		cout << "void EdbShowAlg::Transform_eAli()   SEVERE WARNING:  IF gAli is in wrong order it can be that no showers " << endl;
		cout << "void EdbShowAlg::Transform_eAli()   SEVERE WARNING:  are reconstructed since most implemented algorithms " << endl;
		cout << "void EdbShowAlg::Transform_eAli()   SEVERE WARNING:  rely on the order that plate 2 comes directly behind " << endl;
		cout << "void EdbShowAlg::Transform_eAli()   SEVERE WARNING:  the InBT.   /// DEBUG  TODO  // DEBUG TODO " << endl;
		*/
	

// 	gEDBDEBUGLEVEL=3;
// 	cout << "void EdbShowAlg::Transform_eAli()     ===== eUseAliSub "  << eUseAliSub << endl;

	// IF TO RECREATE THE gALI_SUB in RECONSTRUCTION  or if to use gAli global (slowlier but maybe memory leak safe).
	if (!eUseAliSub)
	{
		if (gEDBDEBUGLEVEL>2) cout << "EdbShowAlg::Transform_eAli   UseAliSub==kFALSE No new eAli_Sub created. Use eAli instead. "<<endl;
		eAli_Sub=eAli;
		eAli_SubNpat=eAli_Sub->Npatterns();  //number of plates 
		if (gEDBDEBUGLEVEL>3) eAli_Sub->Print();
		return;
	}
	
	Int_t npat;
	npat = eAli->Npatterns();  //number of plates 
        
  // has to be deleted in some part of the script outside this function...
  // Dont forget , otherwise memory heap overflow!
  /// DEBUG       if (eAli_Sub) { delete eAli_Sub;eAli_Sub=0;} // original, but keeps not adresses of segment in eAli.
	if (eAli_Sub) { ;} /// do nothing now... let it live... delete eAli_Sub;eAli_Sub=0;}
// 	eAli_Sub = new EdbPVRec();
	

	if (eUseAliSub) {
		if (gEDBDEBUGLEVEL>2) cout << "EdbShowAlg::Transform_eAli   UseAliSub==kTRUE Will now create new eAli_Sub. "<<endl;

/// 		if (eAli_Sub) { delete eAli_Sub;eAli_Sub=0;} Try not to delete it maybe then it works.....
		eAli_Sub = new EdbPVRec();	
// 		cout << "Adress of eAli_Sub " << eAli_Sub << endl;
// 		cout << "eAli_Sub->Npatterns();  " << eAli_Sub->Npatterns() << endl;
// 		eAli_Sub->Print();
	}
		
	
  
// 	cout << "PROBLE THRANFOMING, CAUSE ADRESES ARE DELETED ALSO...." << endl;
//  	cout << "TEMPORAY SOLUTION:     comment the delete 	eAli_Sub  ( will lead to large memory consumption when run for long time" << endl;	
			
			
  // Create SubPattern objects
	EdbSegP* ExtrapolateInitiatorBT=0;
	ExtrapolateInitiatorBT = (EdbSegP*)InitiatorBT->Clone();
  
	Int_t InitiatorBTMCEvt=InitiatorBT->MCEvt();
      
  // Create Variables For ExtractSubpattern boundaries
	Float_t mini[5];Float_t maxi[5];
	//Float_t ExtractSize=1000;// now in fucntion header
	mini[0]=ExtrapolateInitiatorBT->X()-ExtractSize;mini[1]=ExtrapolateInitiatorBT->Y()-ExtractSize;
	maxi[0]=ExtrapolateInitiatorBT->X()+ExtractSize;maxi[1]=ExtrapolateInitiatorBT->Y()+ExtractSize;
	mini[2]=-0.7;mini[3]=-0.7;mini[4]=0.0;
	maxi[2]=0.7;maxi[3]=0.7;maxi[4]=100.0;
  
	EdbPattern* singlePattern;
	Float_t ExtrapolateInitiatorBT_zpos_orig=ExtrapolateInitiatorBT->Z();
      
  // Add the subpatterns in a loop for the plates:
  // in reverse ordering.due to donwstream behaviour (!):
  // (Only downstream is supported now...)
	for (Int_t ii=0; ii<npat; ++ii){
      
		Float_t zpos=eAli->GetPattern(ii)->Z();
          
		ExtrapolateInitiatorBT->PropagateTo(zpos);
        
		mini[0]=ExtrapolateInitiatorBT->X()-ExtractSize;
		mini[1]=ExtrapolateInitiatorBT->Y()-ExtractSize;
		maxi[0]=ExtrapolateInitiatorBT->X()+ExtractSize;
		maxi[1]=ExtrapolateInitiatorBT->Y()+ExtractSize;
        
		singlePattern=(EdbPattern*)eAli->GetPattern(ii)->ExtractSubPattern(mini,maxi,InitiatorBTMCEvt);
		// This sets PID() analogue to (upstream), nut not PID of the BTs !
		singlePattern-> SetID(eAli->GetPattern(ii)->ID());
		// This sets PID() analogue to (upstream), nut not PID of the BTs !     
		singlePattern-> SetPID(eAli->GetPattern(ii)->PID()); 
        
		eAli_Sub->AddPattern(singlePattern);
        
    // Propagate back...! (in order not to change the original BT)
		ExtrapolateInitiatorBT->PropagateTo(ExtrapolateInitiatorBT_zpos_orig);
	}
      
	delete ExtrapolateInitiatorBT;
      
	eAli_SubNpat=eAli_Sub->Npatterns();  //number of plates 


if (gEDBDEBUGLEVEL>2) { 
  cout << "EdbShowAlg::Transform_eAli   eAli_Sub  created."<<endl;
  cout << "Adress of eAli_Sub " << eAli_Sub << endl;
  cout << "eAli_Sub->Npatterns();  " << eAli_Sub->Npatterns() << endl;
}
	return;
}



//______________________________________________________________________________


Bool_t EdbShowAlg::IsInConeTube(EdbSegP* TestingSegment, EdbSegP* StartingSegment, Double_t CylinderRadius, Double_t ConeAngle)
{
	// General Function which returns Bool if the Testing BaeTrack is in a cone defined 
	// by the StartingBaseTrack. In case of starting same Z position, a distance cut of 
	// 20microns is assumed....
	// In case of  TestingSegment==StartingSegment this function should correctly return kTRUE also...
	if (gEDBDEBUGLEVEL>3) cout << "Bool_t EdbShowAlg::IsInConeTube() Test Segment " << TestingSegment << " vs. Starting Segment " << StartingSegment << endl;
	
	// We reject any TestingSegment segments which have lower Z than the StartingSegment .
	if (StartingSegment->Z()>TestingSegment->Z() ) return kFALSE;
	
	TVector3 x1(StartingSegment->X(),StartingSegment->Y(),StartingSegment->Z());
	TVector3 x2(TestingSegment->X(),TestingSegment->Y(),TestingSegment->Z());
	TVector3 direction_x1(StartingSegment->TX()*1300,StartingSegment->TY()*1300,1300);
	
		// u1 is the difference vector of the position!
	TVector3 u1=x2-x1;
  
	Double_t direction_x1_norm= direction_x1.Mag();
	Double_t cosangle=  (direction_x1*u1)/(u1.Mag()*direction_x1_norm);
	Double_t angle = TMath::ACos(cosangle);
	
	// This is the old version of angle calculation. It does not give the same results as in ROOT
	// when use TVector3.Angle(&TVector3). // For this IsInConeTube() we use therefore the ROOT calculation.
	angle=u1.Angle(direction_x1);

  // For the case where the two basetracks have same z position
  // the angle is about 90 degree so it makes no sense to calculate it...
  // therefore we set it artificially to zero:
	if (StartingSegment->Z()==TestingSegment->Z() ) {
		if (gEDBDEBUGLEVEL>3) cout << "same Z position of TestingSegment and StartingSegment, Set angle artificially to zero" << endl;
		angle=0.0;
    // Check here for dR manually:
   	//cout << DeltaR_WithoutPropagation(StartingSegment,TestingSegment) << endl;
    //StartingSegment->PrintNice();
    //TestingSegment->PrintNice();
//   cout << StartingSegment->Flag() << " " << TestingSegment->Flag() << endl;
//   cout << StartingSegment->P() << " " << TestingSegment->P() << endl;
		
		// Check for position distance for 20microns if 
		// Testing Segment is in same Z as StartingSegment
		if (gEDBDEBUGLEVEL>3) cout << "Check for position distance for 20microns if Testing Segment is in same Z as StartingSegment" << endl;
		if (gEDBDEBUGLEVEL>3) cout << "DeltaR_WithoutPropagation(StartingSegment,TestingSegment) = "<< DeltaR_WithoutPropagation(StartingSegment,TestingSegment) << endl;
		if (DeltaR_WithoutPropagation(StartingSegment,TestingSegment)<20) return kTRUE;
		if (DeltaR_WithoutPropagation(StartingSegment,TestingSegment)>=20) return kFALSE;
	}
	
  /// Outside if angle greater than ConeAngle (to be fulfilled for Cone and Tube in both cases)
	if (gEDBDEBUGLEVEL>3) cout << "Check if AngleVector now within the ConeAngleVector (<"<< ConeAngle<<"): " <<   angle << endl;
	if (angle>ConeAngle) { return kFALSE;	}
  
  /// if angle smaller than ConeAngle, then you can differ between Tuberadius and CylinderRadius
	Double_t TubeDistance = 1.0/direction_x1_norm  *  ( (x2-x1).Cross(direction_x1) ).Mag();
	
	if (gEDBDEBUGLEVEL>3) cout << "Check if TestingSegment is now within the Tube (<"<< CylinderRadius<<"): " <<   TubeDistance << endl;
	
	if (TubeDistance>CylinderRadius) {
		return kFALSE;
	}
    
	return kTRUE;
}



//______________________________________________________________________________

void EdbShowAlg::Initialize()
{
  
	return;
}
//______________________________________________________________________________

void EdbShowAlg::Execute()
{
	cout << "EdbShowAlg::Execute()-----------------------------------------------------------------------" << endl;
	return;
}
 
//______________________________________________________________________________
 
void EdbShowAlg::Finalize()
{
  
	return;
}

//______________________________________________________________________________
    
void EdbShowAlg::Print()
{    
	cout << "EdbShowAlg::Print()" << endl;
	cout << eAlgName << "  ;  AlgValue=  " <<  eAlgName << "  ." << endl;
	for (int i=0; i<10; i++) cout << eParaString[i] << "=" << eParaValue[i] << ";  ";
	cout << eFirstPlate_eAliPID << "  " <<  eLastPlate_eAliPID << "  " << eMiddlePlate_eAliPID << "  " << eNumberPlate_eAliPID << "  " << endl;
	cout << "UseAliSub=  " <<  eUseAliSub << "  ." << endl;
	cout << "EdbShowAlg::Print()...done." << endl;
	return;
}

//______________________________________________________________________________
    
void EdbShowAlg::PrintParameters()
{    
	cout << "EdbShowAlg::PrintParameters()" << endl;
	cout << eAlgName<< " :" << endl;
	for (int i=0; i<5; i++) cout << setw(6) << eParaString[i];
	cout << endl;
	for (int i=0; i<5; i++) cout << setw(6) << eParaValue[i];
	cout <<  " ."<<endl;
	return;
}

//______________________________________________________________________________
    
void EdbShowAlg::PrintParametersShort()
{    
	cout << eAlgName<< " :";
	for (int i=0; i<5; i++) cout << setw(6) << eParaValue[i];
	cout <<  " ."<<endl;
	return;
}

//______________________________________________________________________________

void EdbShowAlg::PrintMore()
{   
 cout << "EdbShowAlg::PrintMore()" << endl;
 cout << "eInBTArray->GetEntries();=  " <<  eInBTArray->GetEntries() << "  ." << endl;
 cout << "EdbShowAlg::PrintMore()...done." << endl;
	return;
}

//______________________________________________________________________________


void EdbShowAlg::PrintAll()
{   
	return;
}


//______________________________________________________________________________


Double_t EdbShowAlg::DeltaR_NoPropagation(EdbSegP* s,EdbSegP* stest)
{
	// SAME function as DeltaR_WithoutPropagation !!!
  return DeltaR_WithoutPropagation(s,stest);
}

//______________________________________________________________________________

Double_t EdbShowAlg::DeltaR_WithoutPropagation(EdbSegP* s,EdbSegP* stest)
{
	return TMath::Sqrt((s->X()-stest->X())*(s->X()-stest->X())+(s->Y()-stest->Y())*(s->Y()-stest->Y()));
}

//______________________________________________________________________________

Double_t EdbShowAlg::DeltaR_WithPropagation(EdbSegP* s,EdbSegP* stest)
{
	if (s->Z()==stest->Z()) return TMath::Sqrt((s->X()-stest->X())*(s->X()-stest->X())+(s->Y()-stest->Y())*(s->Y()-stest->Y()));
	Double_t zorig;	Double_t dR;
	zorig=s->Z();	s->PropagateTo(stest->Z());
	dR=TMath::Sqrt( (s->X()-stest->X())*(s->X()-stest->X())+(s->Y()-stest->Y())*(s->Y()-stest->Y()) );
	s->PropagateTo(zorig);
	return dR;
}
    
//______________________________________________________________________________

Double_t EdbShowAlg::DeltaTheta(EdbSegP* s1,EdbSegP* s2)
{
	// Be aware that this DeltaTheta function returns the abs() difference between the
	// ABSOLUTE values of dTheta!!! (not componentwise!
	Double_t tx1,tx2,ty1,ty2;
	tx1=s1->TX();	tx2=s2->TX();	ty1=s1->TY();	ty2=s2->TY();
	Double_t dt= TMath::Abs(TMath::Sqrt(tx1*tx1+ty1*ty1) - TMath::Sqrt(tx2*tx2+ty2*ty2));
	return dt;
}

//______________________________________________________________________________
    

Double_t EdbShowAlg::DeltaThetaComponentwise(EdbSegP* s1,EdbSegP* s2)
{
	// Be aware that this DeltaTheta function returns the difference between the
	// component values of dTheta!!!
	// Acutally this function should be the normal way to calculate dTheta correctly...
	Double_t tx1,tx2,ty1,ty2;
	tx1=s1->TX();	tx2=s2->TX(); 	ty1=s1->TY();	ty2=s2->TY();
	Double_t dt= TMath::Sqrt( (tx1-tx2)*(tx1-tx2) + (ty1-ty2)*(ty1-ty2) );
	return dt; 
}
//______________________________________________________________________________
Double_t EdbShowAlg::DeltaThetaSingleAngles(EdbSegP* s1,EdbSegP* s2)
{
	// SAME function as DeltaThetaComponentwise !!!
	return DeltaThetaComponentwise(s1,s2);
}

//______________________________________________________________________________

Double_t EdbShowAlg::GetSpatialDist(EdbSegP* s1,EdbSegP* s2)
{
  // Mainly Z values should dominate... since the are at the order of 10k microns and x,y of 1k microns  
  Double_t x1,x2,y1,y2,z1,z2;
  x1=s1->X();  x2=s2->X();  y1=s1->Y();  y2=s2->Y(); z1=s1->Z(); z2=s2->Z();
  Double_t dist= TMath::Sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2)  );
  //cout << "dist = "  <<  dist << endl;
  return dist;
}
//______________________________________________________________________________


 Double_t EdbShowAlg::GetMinimumDist(EdbSegP* seg1,EdbSegP* seg2){
       // calculate minimum distance of 2 lines.
       // use the data of (the selected object)->X(), Y(), Z(), TX(), TY().
       // means, if the selected object == segment, use the data of the segment. or it == track, the use the fitted data.
       // original code from Tomoko Ariga
       // double calc_dmin(EdbSegP *seg1, EdbSegP *seg2, double *dminz = NULL){
 
 double x1,y1,z1,ax1,ay1;
 double x2,y2,z2,ax2,ay2;
 double s1,s2,s1bunsi,s1bunbo,s2bunsi,s2bunbo;
 double p1x,p1y,p1z,p2x,p2y,p2z,p1p2;
 
 if(seg1->ID()==seg2->ID()&&seg1->PID()==seg2->PID()) return 0.0;
 
 x1 = seg1->X(); y1 = seg1->Y(); z1 = seg1->Z();
 ax1= seg1->TX(); ay1= seg1->TY();
 
 x2 = seg2->X(); y2 = seg2->Y(); z2 = seg2->Z();
 ax2= seg2->TX(); ay2= seg2->TY();
 
 s1bunsi=(ax2*ax2+ay2*ay2+1)*(ax1*(x2-x1)+ay1*(y2-y1)+z2-z1) - (ax1*ax2+ay1*ay2+1)*(ax2*(x2-x1)+ay2*(y2-y1)+z2-z1);
 s1bunbo=(ax1*ax1+ay1*ay1+1)*(ax2*ax2+ay2*ay2+1) - (ax1*ax2+ay1*ay2+1)*(ax1*ax2+ay1*ay2+1);
 s2bunsi=(ax1*ax2+ay1*ay2+1)*(ax1*(x2-x1)+ay1*(y2-y1)+z2-z1) - (ax1*ax1+ay1*ay1+1)*(ax2*(x2-x1)+ay2*(y2-y1)+z2-z1);
 s2bunbo=(ax1*ax1+ay1*ay1+1)*(ax2*ax2+ay2*ay2+1) - (ax1*ax2+ay1*ay2+1)*(ax1*ax2+ay1*ay2+1);
 s1=s1bunsi/s1bunbo; s2=s2bunsi/s2bunbo;
 p1x=x1+s1*ax1; p1y=y1+s1*ay1; p1z=z1+s1*1;
 p2x=x2+s2*ax2; p2y=y2+s2*ay2; p2z=z2+s2*1;
 p1p2=sqrt( (p1x-p2x)*(p1x-p2x)+(p1y-p2y)*(p1y-p2y)+(p1z-p2z)*(p1z-p2z) );
 
 return p1p2;
 }
 
 //______________________________________________________________________________
 //______________________________________________________________________________
 
 
 
//______________________________________________________________________________

EdbShowAlg_GS::EdbShowAlg_GS()
{
  // Default Constructor
  Log(2,"EdbShowAlg_GS::EdbShowAlg_GS","EdbShowAlg_GS:: Default Constructor");
  
  
  cout << "Howto do:  " << endl;
  cout << "	Alg = new EdbShowAlg_GS();" << endl;
  cout << "	Alg->SetEdbPVRec(EdbPVRec*);" << endl;
  cout << "	Alg->SetInVtx(EdbVertex*)" << endl;
  cout << "	Alg->Execute();" << endl;
  cout << "	Alg->GetRecoShowerArray();   .... Returns you the compatible Pair Segments (stored as EdbTrackP*)" << endl;
  cout << "	" << endl;
  
  
  // Reset all:
  Set0();
  
  eAlgName="GS";
  eAlgValue=999; // see default.par_SHOWREC for labeling (labeling identical with ShowRec program)
  
  eInVtxArrayN=0;
  eInVtxArray=NULL;
  
  eInBTArrayN=0;
  eInBTArray=NULL;
  
  //  Init with values according to GS Alg:
  Init();
  
  
  
}

//______________________________________________________________________________

EdbShowAlg_GS::~EdbShowAlg_GS()
{
  // Default Destructor
  Log(2,"EdbShowAlg_GS::~EdbShowAlg_GS","EdbShowAlg_GS:: Default Destructor");
}

//______________________________________________________________________________

void EdbShowAlg_GS::Init()
{  
  Log(2,"EdbShowAlg_GS::EdbShowAlg_GS","Init()");
  
  //  Init with values according to GS Alg:
  //  Took over from "FindGamma.C" script I develoved before:
  // IP CUT; this cut is used for the -better- IP of both BTs to Vertex/BT
  eParaValue[0]=250;
  eParaString[0]="PARA_GS_CUT_dIP";  
  // min minDist.e between pair BTs
  eParaValue[1]=40;
  eParaString[1]="PARA_GS_CUT_dMin";
  // min dR between pair BTs
  eParaValue[2]=60;
  eParaString[2]="PARA_GS_CUT_dR";
  // max Z distance between pair BTs and Vertex/BT
  eParaValue[3]=19000;
  eParaString[3]="PARA_GS_CUT_dZ";
  // max Angle between pair BTs
  eParaValue[4]=0.060;
  eParaString[4]="PARA_GS_CUT_dtheta";
  // max plates difference between pair BTs
  eParaValue[5]=0;
  eParaString[5]="PARA_GS_CUT_PIDDIFF";
  
  
  if (!eRecoShowerArray) eRecoShowerArray= new TObjArray(999);
  eRecoShowerArrayN=0;
  return;
}

//______________________________________________________________________________


void EdbShowAlg_GS::Initialize()
{  
  Log(2,"EdbShowAlg_GS::EdbShowAlg_GS","Initialize()");
  return;
}

//______________________________________________________________________________




void EdbShowAlg_GS::SetInVtx( EdbVertex* vtx )
{
  Log(2,"EdbShowAlg_GS::SetInVtx","SetInVtx()");
 if (!eInVtxArray)  eInVtxArray = new TObjArray();
 eInVtxArray->Add(vtx);
 ++eInVtxArrayN;
 return;
}
 


//______________________________________________________________________________


void EdbShowAlg_GS::Convert_InVtxArray_To_InBTArray()
{
  Log(2,"EdbShowAlg_GS::EdbShowAlg_GS","Convert_InVtxArray_To_InBTArray()");
  if (eInBTArray!=NULL) {eInBTArray->Clear();  cout << " eInBTArray->Clear();" << endl; }
  if (eInBTArray==NULL) {eInBTArray = new TObjArray();cout << " eInBTArray = new TObjArray()" << endl;  } 
  
  if (eInVtxArray==NULL) cout << "NO  eInVtxArray  " << endl;
  EdbVertex* vtx;
  cout << "EdbVertex* vtx;" << endl;
  cout <<eInVtxArrayN << endl;
  cout << "now eInVtxArray->Print();" << endl;
  eInVtxArray->Print();
  cout << eInVtxArray->GetEntries() << endl;
  
  Log(2,"EdbShowAlg_GS::EdbShowAlg_GS","Convert_InVtxArray_To_InBTArray()...start loop");
  for (Int_t i=0; i<eInVtxArrayN; i++) {
    vtx= (EdbVertex*)eInVtxArray->At(i);
    //vtx->Print();
    EdbSegP* seg = new EdbSegP(i,vtx->X(),vtx->Y(),0,0);
    seg->SetZ(vtx->Z());
    // vtx can have by default initialization MCEvt==0,
    // this we dont want, in case: we set MCEvt of vtx from 0 to -999
    if (vtx->MCEvt()==0) vtx->SetMC(-999);
    seg->SetMC(vtx->MCEvt(),vtx->MCEvt());
    seg->SetFlag(0);
    //seg->Print();
    eInBTArray->Add(seg);
  }
  
  eInBTArrayN=eInBTArray->GetEntries();
  if (gEDBDEBUGLEVEL>2) cout << "EdbShowAlg_GS::Convert_InVtxArray_To_InBTArray   Converted " << eInBTArrayN << "InVtx to InBT." << endl;
  Log(2,"EdbShowAlg_GS::EdbShowAlg_GS","Convert_InVtxArray_To_InBTArray()...done.");
  return;
}

//______________________________________________________________________________


Bool_t EdbShowAlg_GS::CheckPairDuplications(Int_t SegPID,Int_t SegID,Int_t Seg2PID,Int_t Seg2ID,TArrayI* SegmentPIDArray,TArrayI* SegmentIDArray,TArrayI* Segment2PIDArray,TArrayI* Segment2IDArray)
{
  
  for (Int_t i=0; i<eRecoShowerArrayN; i++) {
    // PID and ID of Seg and Seg2 to be exchanged for duplications
    if ( SegPID==Segment2PIDArray->At(i) && Seg2PID==SegmentPIDArray->At(i) && SegID==Segment2IDArray->At(i) && Seg2ID==SegmentIDArray->At(i)) {
      //cout << "Found duplocation for... return true"<<endl;
      return kTRUE;
    }
  }
  return kFALSE;
}


//______________________________________________________________________________



void EdbShowAlg_GS::Execute()
{
  Log(2,"EdbShowAlg_GS::Execute","Execute()");
  cout << "EdbShowAlg_GS::Execute()" << endl;
  Log(2,"EdbShowAlg_GS::Execute","Execute()   DOING MAIN SHOWER RECONSTRUCTION HERE");
  
  
  if (!eInBTArray) {
    Log(2,"EdbShowAlg_GS::Execute","Execute()   No eInBTArray. Check for eInVtxArray:");
    // Check if there is an Convert_InVtxArray_To_InBTArray();
    if (!eInVtxArray) {
      Log(2,"EdbShowAlg_GS::Execute","Execute()   No eInVtxArray. RETURN.");
     return; 
    }
    cout << "eInVtxArray there. Converting to InBTArray() now. "<< endl;
    Convert_InVtxArray_To_InBTArray();
  }
  
  EdbSegP* InBT=NULL;
  
//   EdbShowerP* RecoShower;
  EdbTrackP* RecoShower;
  
  Int_t     STEP=-1;
  if (eFirstPlate_eAliPID-eLastPlate_eAliPID<0) STEP=1;
  if (gEDBDEBUGLEVEL>2) cout << "EdbShowAlg_GS::Execute--- STEP for patternloop direction =  " << STEP << endl;
  if (gEDBDEBUGLEVEL>2) cout << "EdbShowAlg_GS::Execute--- eRecoShowerArrayN =  " << eRecoShowerArrayN << endl;
  
  //--- Loop over InBTs:
  if (gEDBDEBUGLEVEL>=2) cout << "EdbShowAlg_GS::Execute    Loop over InBTs:" << endl;
  
  // Since eInBTArray is filled in ascending ordering by zpositon
  // We use the descending loop to begin with BT with lowest z first.
  // InBT doest have to be necessary a real BaseTrack, it can also be a vertex (with its positions and angle zero) !!!
  for (Int_t i=eInBTArrayN-1; i>=0; --i) {
    
    // CounterOutPut
    if (gEDBDEBUGLEVEL==2) if((i%100)==0) cout << eInBTArrayN <<" InBT in total, still to do:"<<Form("%4d",i)<< "\r\r\r\r"<<flush;
    
    // Get InitiatorBT from eInBTArray  InBT
    InBT=(EdbSegP*)eInBTArray->At(i);
    if (gEDBDEBUGLEVEL>2) InBT->PrintNice();
    
    //-----------------------------------
    // 1) Make local_gAli with cut parameters:
    //-----------------------------------
    // Transform (make size smaller, extract only events having same MC) the  eAli  object:    
    Transform_eAli(InBT,999999);
    if (gEDBDEBUGLEVEL>2)  eAli_Sub->Print();
    

    //-----------------------------------
    // 2) FindPairs
    //-----------------------------------
    TObjArray* Pairs = FindPairs(InBT,eAli_Sub);
    if (gEDBDEBUGLEVEL>2) cout << "TObjArray* Pairs = FindPairs(InBT,eAli_Sub); done. Entries= " << Pairs->GetEntries() << endl;
    
      
    //-----------------------------------
    // 2) Clear Found Pairs
    //-----------------------------------
    TObjArray* CleanPairs = CheckCleanPairs( InBT, Pairs);
    for (int j=0; j<CleanPairs->GetEntries(); ++j) {
      cout << "j= " << j << endl;
      EdbTrackP* pair=(EdbTrackP*)CleanPairs->At(j);
      eRecoShowerArray->Add(pair);
       ++eRecoShowerArrayN;
    }
    
    
    
  } // Loop over InBT
  cout << " Loop over InBT finished."<< endl;
  
  
  cout << " eRecoShowerArray=  " <<  eRecoShowerArray << endl;
  cout << " eRecoShowerArrayN=  " <<  eRecoShowerArrayN << endl;

  for (Int_t i=0; i <eRecoShowerArray->GetEntries();  i++) {
//   EdbShowerP* sh = (EdbShowerP* )eRecoShowerArray->At(i);
//   sh->PrintSegments();
  EdbTrackP* sh = (EdbTrackP* )eRecoShowerArray->At(i);
  sh->PrintNice();
  }
  
  Log(2,"EdbShowAlg_GS::Execute","Execute()...done.");
  return;
}

//______________________________________________________________________________



TObjArray* EdbShowAlg_GS::FindPairs(EdbSegP* InBT, EdbPVRec* eAli_Sub) 
{
  if (gEDBDEBUGLEVEL>2) cout << "Starting FindPairs(InBT,eAli_Sub) now" << endl;
  
  TObjArray* RecoShowerArray= new TObjArray(99);
  RecoShowerArray->Clear();
  Int_t RecoShowerArrayN=0;
  TArrayI* SegmentPIDArray = new TArrayI(9999);	TArrayI* SegmentIDArray = new TArrayI(9999);
  TArrayI* Segment2PIDArray = new TArrayI(9999);	TArrayI* Segment2IDArray = new TArrayI(9999);
  EdbSegP* Segment=NULL;
  EdbSegP* Segment2=NULL;
  Float_t x_av,y_av,z_av,tx_av,ty_av,distZ;
  EdbSegP* Segment_Sum=new EdbSegP(0,0,0,0,0,0);
  Float_t	 IP_Pair_To_InBT=0;
  
  if (NULL==InBT) { 
    EdbSegP* InBT= new EdbSegP();
    InBT->SetX(0);InBT->SetY(0);InBT->SetZ(0);
    InBT->SetTX(0);InBT->SetTY(0);
    InBT->SetMC(-999,-999);
  }
  
  //-----------------------------------
  // 2) Loop over (whole) eAli, check BT for Cuts
  // eAli_Sub
  // Loop structure:
  // Loop over plates [0..NPatterns-1] for pattern one 
  // Loop over plates [0..NPatterns-1] for pattern two
  // Take only plate pairings for |PID diff|<=3
  // Loop over all BT of pattern one
  // Loop over all BT of pattern two
  
  // This doesnt yet distinguish the FIRSTPLATE, MIDDLEPLATE, LATPLATE,NUMBERPLATES 
  // labelling, this will be built in later....
  //-----------------------------------    
  
  Int_t npat=eAli_Sub->Npatterns()-1;
  Int_t pat_one_bt_cnt_max,pat_two_bt_cnt_max=0;
  EdbPattern* pat_one=0;
  EdbPattern* pat_two=0;
  
  for (Int_t pat_one_cnt=0; pat_one_cnt<npat; ++pat_one_cnt) {
    pat_one=(EdbPattern*)eAli_Sub->GetPattern(pat_one_cnt);
    pat_one_bt_cnt_max=eAli_Sub->GetPattern(pat_one_cnt)->GetN();
    
    // Check if dist Z to vtx (BT) is ok:
    distZ=pat_one->Z()-InBT->Z();
    if (distZ>eParaValue[3]) continue;
    if (distZ<1000) continue;
      
   if (gEDBDEBUGLEVEL>2) cout << "pat_one_cnt=" << pat_one_cnt << "  pat_one->Z() = " << pat_one->Z() << " pat_one_bt_cnt_max= "<< pat_one_bt_cnt_max <<endl;
    
    
    for (Int_t pat_two_cnt=0; pat_two_cnt<npat; ++pat_two_cnt) { 
      
      // Now apply cut conditions: GS  GAMMA SEARCH Alg:
      if (TMath::Abs(pat_one_cnt-pat_two_cnt)>eParaValue[5]) continue;
      
      pat_two=(EdbPattern*)eAli_Sub->GetPattern(pat_two_cnt);
      pat_two_bt_cnt_max=eAli_Sub->GetPattern(pat_two_cnt)->GetN();
      
      
      
      if (gEDBDEBUGLEVEL>2) cout << "	pat_two_cnt=" << pat_two_cnt << "  pat_two->Z() = " << pat_two->Z() << " pat_two_bt_cnt_max= "<< pat_two_bt_cnt_max <<endl;
      
      for (Int_t pat_one_bt_cnt=0; pat_one_bt_cnt<pat_one_bt_cnt_max; ++pat_one_bt_cnt) {
	Segment =  (EdbSegP*)pat_one->GetSegment(pat_one_bt_cnt);
	
	for (Int_t pat_two_bt_cnt=0; pat_two_bt_cnt<pat_two_bt_cnt_max; ++pat_two_bt_cnt) {  
	  
	  Segment2 = (EdbSegP*)pat_two->GetSegment(pat_two_bt_cnt);
	  if (Segment2==Segment) continue;
	  if (Segment2->ID()==Segment->ID()&&Segment2->PID()==Segment->PID()) continue;
	  
	  // At first:  Check for already duplicated pairings:
	  if (CheckPairDuplications(Segment->PID(),Segment->ID(),Segment2->PID(),Segment2->ID(), SegmentPIDArray,SegmentIDArray,Segment2PIDArray,Segment2IDArray)) continue;
	  
	  // Now apply cut conditions: GS  GAMMA SEARCH Alg  --------------------
	  // if InBT is flagged as MC InBT, take care that only BG or same MC basetracks are taken:
	  if (InBT->MCEvt()>0) if (Segment->MCEvt()>0&&Segment2->MCEvt()>0) if (Segment->MCEvt()!=Segment2->MCEvt()) continue;
	  if (InBT->MCEvt()>0) if (Segment->MCEvt()>0&&Segment2->MCEvt()>0) if (Segment->MCEvt()!=InBT->MCEvt()) continue;
	  if (InBT->MCEvt()>0) if (Segment->MCEvt()>0&&Segment2->MCEvt()>0) if (Segment2->MCEvt()!=InBT->MCEvt()) continue;
	  
	  // In case of two MC events, check for e+ e- pairs
	  // Only if Parameter is set to choose different Flag() pairs:
	  if (InBT->MCEvt()>0 && eParaValue[5]==1) 
	    if (Segment->MCEvt()>0&&Segment2->MCEvt()>0) 
	      if ((Segment2->Flag()+Segment->Flag())!=0) continue;
	  
	  // a) Check dR between tracks:
	  if (DeltaR_WithPropagation(Segment,Segment2)>eParaValue[2]) continue;
	  // b) Check dT between tracks:
	  if (DeltaThetaSingleAngles(Segment,Segment2)>eParaValue[4]) continue;
	  // c) Check dMinDist between tracks:
	  if (GetMinimumDist(Segment,Segment2)>eParaValue[1]) continue;
	  
	  // d) Check if dist Z to vtx (BT) is ok:
	  distZ=Segment->Z()-InBT->Z();
	  if (distZ>eParaValue[3]) continue;
	  
	  x_av=Segment2->X()+(Segment->X()-Segment2->X())/2.0;
	  y_av=Segment2->Y()+(Segment->Y()-Segment2->Y())/2.0;
	  z_av=Segment2->Z()+(Segment->Z()-Segment2->Z())/2.0;
	  tx_av=Segment2->TX()+(Segment->TX()-Segment2->TX())/2.0;
	  ty_av=Segment2->TY()+(Segment->TY()-Segment2->TY())/2.0;
	  Segment_Sum->SetX(x_av);Segment_Sum->SetY(y_av);
	  Segment_Sum->SetTX(tx_av);Segment_Sum->SetTY(ty_av);
	  Segment_Sum->SetZ(z_av);
	  
	  // d) Check if IP to vtx (BT) is ok:
// 	  IP_Pair_To_InBT=CalcIP(Segment_Sum, InBT->X(),InBT->Y(),InBT->Z());
// 	  if (IP_Pair_To_InBT>eParaValue[0]) continue;
	  
	  Float_t IP_Pair_To_InBT_Seg	=CalcIP(Segment, InBT->X(),InBT->Y(),InBT->Z());
	  Float_t IP_Pair_To_InBT_Seg2	=CalcIP(Segment2, InBT->X(),InBT->Y(),InBT->Z());
	  
	  // d) Check if IP to vtx (BT) is ok:
	  if (TMath::Min(IP_Pair_To_InBT_Seg,IP_Pair_To_InBT_Seg2)>eParaValue[0]) continue;
	  
	  cout << "IP_Pair_To_InBT_Seg= " << IP_Pair_To_InBT_Seg << endl;
	  cout << "IP_Pair_To_InBT_Seg2= " << IP_Pair_To_InBT_Seg2 << endl;
	  
	  // e) Check if this is not a possible fake doublet which is 
	  //	sometimes caused by view overlap in the scanning:
	  //cout << " Check for IsPossibleFakeDoublet by view overlap" << endl;
 	  if (IsPossibleFakeDoublet(Segment,Segment2) ) continue;
	  // end of    cut conditions: GS  GAMMA SEARCH Alg  --------------------
	  // 	  


	  // Chi2 Variable:
	  Float_t dminDist=GetMinimumDist(Segment,Segment2);
	  Float_t dtheta=DeltaThetaSingleAngles(Segment,Segment2);
	  // preliminary meand and sigma values (taken from 1Gev, with highpur cutset), to be checked if they are the same for 0.5,1,2,4 GeV Photons
	  Float_t GammaChi2 =  ((IP_Pair_To_InBT-80)*(IP_Pair_To_InBT-80)/60/60)+((dminDist-3.5)*(dminDist-3.5)/4.7/4.7)+((dtheta-0.021)*(dtheta-0.021)/0.012/0.012);
	  
	  
	  	  
	  if (gEDBDEBUGLEVEL>2) {
	    cout << "Pair has passed all cuts w.r.t to InBT:" << endl;	  
	    cout << "IP_Pair_To_InBT  = " << IP_Pair_To_InBT << endl;
	    cout << "GetMinimumDist(Segment,Segment2)  = " << GetMinimumDist(Segment,Segment2) << endl;
	    cout << "CalcIP(Segment_Sum,InBT)  = " << IP_Pair_To_InBT << endl;
	    cout << "GetSpatialDist(Segment_Sum,InBT)  = " << GetSpatialDist(Segment_Sum,InBT) << endl;
	    cout << "GammaChi2 = " << GammaChi2 << endl;
	  }
	  
	  SegmentPIDArray->AddAt(Segment->PID(),RecoShowerArrayN);
	  SegmentIDArray->AddAt(Segment->ID(),RecoShowerArrayN);
	  Segment2PIDArray->AddAt(Segment2->PID(),RecoShowerArrayN);
	  Segment2IDArray->AddAt(Segment2->ID(),RecoShowerArrayN);

	  
	  // Create new EdbTrackP Object for storage;
	  EdbTrackP* RecoShower = new EdbTrackP();
	  RecoShower -> AddSegment(Segment);
	  RecoShower -> AddSegment(Segment2);  
	   // Set X and Y and Z values: /Take lower Z of both BTs)
	   RecoShower->SetZ(TMath::Min(Segment->Z(),Segment2->Z()));
	   RecoShower->SetX(Segment_Sum->X());
	   RecoShower->SetY(Segment_Sum->Y());
	   RecoShower->SetTX(Segment_Sum->TX());
	   RecoShower->SetTY(Segment_Sum->TY());
	   RecoShower->SetMC(InBT->MCEvt(),InBT->MCEvt());
	   RecoShower->SetID(RecoShowerArrayN);
	   RecoShower->SetPID(Segment->PID());
	  RecoShower ->PrintNice();

	  cout <<"------------"<< endl;
	  
	  
	  // Add Shower to interim  Array:
	  RecoShowerArray->Add(RecoShower);
	  ++RecoShowerArrayN;
	  
	}
      }
    }
  }  //for (Int_t pat_one_cnt=0; ...
  
  cout << "EdbShowAlg_GS::FindPairs For the InBT/Vtx at __" << InBT << "__, we have found "  <<  RecoShowerArray->GetEntries()  << " compatible pairs in the PVRec volume." << endl;
  
  // Delete unnecessary objects:
  // important, else memory overflow!!
  delete SegmentPIDArray;delete Segment2PIDArray;
  delete SegmentIDArray;delete Segment2IDArray;	
  
  return RecoShowerArray;
}

//______________________________________________________________________________


TObjArray* EdbShowAlg_GS::CheckCleanPairs(EdbSegP* InBT, TObjArray* RecoShowerArray)
{
   
   cout << "CheckCleanPairs"  <<endl;
   if (NULL==RecoShowerArray) return NULL; 
   
   TObjArray* NewRecoShowerArray= new TObjArray(999999);
   NewRecoShowerArray->Clear();
   int NewRecoShowerArrayN=0;
   
   EdbTrackP* TrackPair1=NULL;
   EdbTrackP* TrackPair2=NULL;
  
    int ntrack=RecoShowerArray->GetEntriesFast();
   cout << ntrack << endl;
   
      for (Int_t pat_one_cnt=0; pat_one_cnt<ntrack; ++pat_one_cnt) {
	cout << "Doing pat_one_cnt = " << pat_one_cnt << endl;
     // (if -1) then the last one is not checked
     TrackPair1=(EdbTrackP*)RecoShowerArray->At(pat_one_cnt);
     bool taketrack1=true;
     for (Int_t pat_two_cnt=pat_one_cnt; pat_two_cnt<ntrack; ++pat_two_cnt) { 
       cout << "Doing pat_two_cnt = " << pat_two_cnt << endl;
       //if only one track at all take it anyway:
       if (ntrack==1) continue;
       TrackPair2=(EdbTrackP*)RecoShowerArray->At(pat_two_cnt);
       //      cout << pat_one_cnt << "  " << pat_two_cnt << endl;
       //      cout << pat_one_cnt << "  " << pat_two_cnt << endl;
       
       // Check  if track1 has a track before (smaller Z value)
       // and if so, if dtheta is smaller than 0.1:
       // If both is so, then track 1 is NOT taken for final array:
       //       cout << TrackPair1->Z() << "  " <<  TrackPair2->Z()  << endl;
       EdbSegP* s1=(EdbSegP* )TrackPair1->GetSegment(0);  
//        s1->PrintNice(); 
       EdbSegP* s2=(EdbSegP* )TrackPair2->GetSegment(0);  
//        s2->PrintNice(); 
       if (TrackPair1->Z()>TrackPair2->Z() && DeltaThetaSingleAngles((EdbSegP*)s1,(EdbSegP*)s2)<0.05) taketrack1=false;
       if (!taketrack1) break;
     }
     
     cout << "taketrack1 =  " << taketrack1 << endl;
     if (!taketrack1) continue;
     
     // Add TrackPair1   
     NewRecoShowerArray->Add(TrackPair1);
     ++NewRecoShowerArrayN;
     
     
     // Print
     if(TrackPair1->N()<2) cout << "This Track has only ONE entry" << endl;
     
    EdbSegP* s1=(EdbSegP* )TrackPair1->GetSegment(0);  
    s1->PrintNice(); 
    EdbSegP* s2=(EdbSegP* )TrackPair1->GetSegment(1);  
    s2->PrintNice(); 
    
    
    cout <<  s1->MCEvt() << "  " <<  s2->MCEvt() << "  " << s1->Flag() << "  " << s2->Flag() << "  " << s1->P() << "  " << s2->P() << "  " <<  "  " << s1->Z() << "  " << s2->Z() << "  " <<endl;
    cout<< "--------"<<endl;
   }
   cout << " From " <<ntrack << "  originally, there are now after Zposition and overlap cuts: " << NewRecoShowerArrayN << " left." << endl;
   
  
 return NewRecoShowerArray; 
}





//______________________________________________________________________________

void EdbShowAlg_GS::Finalize()
{
  // do nothing yet.
}
  //______________________________________________________________________________
  
  
  double EdbShowAlg_GS::CalcIP(EdbSegP *s, double x, double y, double z){
    // Calculate IP between a given segment and a given x,y,z.
    // return the IP value.
    double ax = s->TX();
    double ay = s->TY();
    double bx = s->X()-ax*s->Z();
    double by = s->Y()-ay*s->Z();
    double a;	double r;
    double xx,yy,zz;
    a = (ax*(x-bx)+ay*(y-by)+1.*(z-0.))/(ax*ax+ay*ay+1.);
    xx = bx +ax*a;
    yy = by +ay*a;
    zz = 0. +1.*a;
    r = sqrt((xx-x)*(xx-x)+(yy-y)*(yy-y)+(zz-z)*(zz-z));
    return r;
  }	
  double EdbShowAlg_GS::CalcIP(EdbSegP *s, EdbVertex *v){
    // calculate IP between a given segment and a given vertex.
    // return the IP value.
    // this is used for IP cut.
    
    // if vertex is not given, use the selected vertex.
    // if(v==NULL) v=gEDA->GetSelectedVertex();
    
    if(v==NULL) return -1.;
    return CalcIP(s, v->X(), v->Y(), v->Z());
  }
  
//______________________________________________________________________________
  
Bool_t EdbShowAlg_GS::IsPossibleFakeDoublet(EdbSegP* s1,EdbSegP* s2) {
  if (TMath::Abs(s1->X()-s2->X())<1) return kTRUE;  // minimum distance of 1micron
  if (TMath::Abs(s1->Y()-s2->Y())<1) return kTRUE;// minimum distance of 1micron
  if (TMath::Abs(s1->TX()-s2->TX())<0.005) return kTRUE;// minimum angle of 5mrad
  if (TMath::Abs(s1->TY()-s2->TY())<0.005) return kTRUE;// minimum angle of 5mrad
  return kFALSE;
}