#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class EdbShowerRec;
#pragma link C++ class EdbShowAlg;
#pragma link C++ class EdbShowAlg_GS;

#endif
